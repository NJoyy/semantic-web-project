package de.fabianmittmann.semantic.web.project.rdfimport;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * @author Fabian Mittmann
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Ontology {

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class RdfSyntax {
        public static final String RDF_TYPE = "rdf:type";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Classes {
        public static final String AREA = "Area";

        public static final String COUNTRY = "Country";

        public static final String GDP = "GDP";

        public static final String INFLATION = "Inflation";

        public static final String POPULATION = "Population";

        public static final String YEAR = "Year";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Properties {
        public static final String AREA_IN_SQUARE_KILOMETRES = "areaInSquareKilometres";

        public static final String COUNTRY_NAME = "countryName";

        public static final String GDP_VALUE = "gdpValue";

        public static final String POPULATION_VALUE = "populationValue";

        public static final String INFLATION_VALUE = "inflationValue";

        public static final String YEAR_VALUE = "yearValue";
    }

    @NoArgsConstructor(access = AccessLevel.PRIVATE)
    public static class Relations {
        public static final String DETERMINED_FOR_COUNTRY = "determinedForCountry";

        public static final String DETERMINED_IN_YEAR = "determinedInYear";
    }


}
