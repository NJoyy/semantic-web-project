package de.fabianmittmann.semantic.web.project.datareader;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;

/**
 * @author Fabian Mittmann
 */
public class JsonDataReader {

    private static final ObjectMapper objectMapper = new ObjectMapper();

    public List<GdpDataJson> readGdpData() {
        return readData("gdp.json", GdpDataJson.class);
    }

    public List<InflationDataJson> readInflationData() {
        return readData("inflation.json", InflationDataJson.class);
    }

    public List<PopulationDataJson> readPopulationData() {
        return readData("population.json", PopulationDataJson.class);
    }

    public List<AreaDataJson> readAreaData() {
        return readData("area.json", AreaDataJson.class);
    }

    private <T> List<T> readData(@NonNull final String resourceName, @NonNull final Class<T> clazz) {
        try {
            final var file = new File(
                    Objects.requireNonNull(
                            this.getClass().getClassLoader().getResource(resourceName)
                    ).toURI()
            );

            return objectMapper.readValue(
                    file,
                    objectMapper.getTypeFactory().constructCollectionType(List.class, clazz)
            );
        } catch (URISyntaxException | IOException e) {
            throw new IllegalStateException(String.format("reading resource %s failed", resourceName), e);
        }
    }

}
