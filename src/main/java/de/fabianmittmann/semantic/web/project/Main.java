package de.fabianmittmann.semantic.web.project;

import de.fabianmittmann.semantic.web.project.datareader.JsonDataReader;
import de.fabianmittmann.semantic.web.project.rdfimport.TripleStoreImporter;

/**
 * @author Fabian Mittmann
 */
public class Main {

    public static void main(String[] args) {
        final var reader = new JsonDataReader();

        final var gdpData = reader.readGdpData();
        final var inflationData = reader.readInflationData();
        final var populationData = reader.readPopulationData();
        final var areaData = reader.readAreaData();

        final var importer = new TripleStoreImporter();
        importer.executeRdfImport(
                areaData,
                gdpData,
                inflationData,
                populationData
        );
    }

}
