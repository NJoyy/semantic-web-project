package de.fabianmittmann.semantic.web.project.rdfimport;

import de.fabianmittmann.semantic.web.project.datareader.AreaDataJson;
import de.fabianmittmann.semantic.web.project.datareader.GdpDataJson;
import de.fabianmittmann.semantic.web.project.datareader.InflationDataJson;
import de.fabianmittmann.semantic.web.project.datareader.PopulationDataJson;
import lombok.NonNull;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.util.ModelBuilder;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.eclipse.rdf4j.rio.RDFFormat;

import java.io.IOException;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * @author Fabian Mittmann
 */
public class TripleStoreImporter {

    private static final String REPOSITORY_URL = "http://localhost:7200/repositories/semantic-web-project";

    private static final String BASE_URI = "http://example.com/semantic-web-project/";

    private static final String SPECIFIC_RESOURCE_TEMPLATE = BASE_URI + "%s:%s";

    private final Repository repository;

    public TripleStoreImporter() {
        repository = new HTTPRepository(REPOSITORY_URL);
    }

    public void executeRdfImport(@NonNull final List<AreaDataJson> areaData,
                                 @NonNull final List<GdpDataJson> gdpData,
                                 @NonNull final List<InflationDataJson> inflationData,
                                 @NonNull final List<PopulationDataJson> populationData) {
        importOntology();

        CompletableFuture.allOf(
                CompletableFuture.runAsync(() -> importAreaData(areaData)),
                CompletableFuture.runAsync(() -> importGdpData(gdpData)),
                CompletableFuture.runAsync(() -> importInflationData(inflationData)),
                CompletableFuture.runAsync(() -> importPopulationData(populationData))
        ).join();
    }

    private void importOntology() {
        try(final var connection = repository.getConnection()) {
            connection.begin();
            connection.add(
                    this.getClass().getResourceAsStream("/ontology.ttl"),
                    BASE_URI,
                    RDFFormat.TURTLE
            );
            connection.commit();
        } catch (IOException e) {
            throw new IllegalStateException("ontology upload failed", e);
        }
    }

    private void importAreaData(@NonNull final List<AreaDataJson> areaData) {
        try(final var connection = repository.getConnection()) {
            connection.begin();

            final var modelBuilder = new ModelBuilder();
            final var valueFactory = connection.getValueFactory();

            areaData.forEach(entry -> {
                final var countryUri = toCountryUri(entry.getCountryCode());
                final var yearUri = toYearUri(entry.getYear().toString());
                final var areaUri = toAreaUri(UUID.randomUUID().toString());

                addBaseModel(
                        modelBuilder,
                        valueFactory,
                        countryUri,
                        entry.getCountry(),
                        yearUri,
                        entry.getYear()
                );

                modelBuilder.add(
                        valueFactory.createIRI(areaUri),
                        valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                        valueFactory.createIRI(toUri(Ontology.Classes.AREA))
                );
                modelBuilder.add(
                        valueFactory.createIRI(areaUri),
                        valueFactory.createIRI(toUri(Ontology.Properties.AREA_IN_SQUARE_KILOMETRES)),
                        entry.getAreaInSquareKilometres()
                );
                modelBuilder.add(
                        valueFactory.createIRI(areaUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_IN_YEAR)),
                        valueFactory.createIRI(yearUri)
                );
                modelBuilder.add(
                        valueFactory.createIRI(areaUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_FOR_COUNTRY)),
                        valueFactory.createIRI(countryUri)
                );
            });

            final var model = modelBuilder.build();
            connection.add(model);
            connection.commit();
        }
    }

    private void importGdpData(@NonNull final List<GdpDataJson> gdpData) {
        try(final var connection = repository.getConnection()) {
            connection.begin();

            final var modelBuilder = new ModelBuilder();
            final var valueFactory = connection.getValueFactory();

            gdpData.forEach(entry -> {
                final var countryUri = toCountryUri(entry.getCountryCode());
                final var yearUri = toYearUri(entry.getYear().toString());
                final var gdpUri = toGdpUri(UUID.randomUUID().toString());

                addBaseModel(
                        modelBuilder,
                        valueFactory,
                        countryUri,
                        entry.getCountry(),
                        yearUri,
                        entry.getYear()
                );

                modelBuilder.add(
                        valueFactory.createIRI(gdpUri),
                        valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                        valueFactory.createIRI(toUri(Ontology.Classes.GDP))
                );
                modelBuilder.add(
                        valueFactory.createIRI(gdpUri),
                        valueFactory.createIRI(toUri(Ontology.Properties.GDP_VALUE)),
                        entry.getGdp()
                );
                modelBuilder.add(
                        valueFactory.createIRI(gdpUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_IN_YEAR)),
                        valueFactory.createIRI(yearUri)
                );
                modelBuilder.add(
                        valueFactory.createIRI(gdpUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_FOR_COUNTRY)),
                        valueFactory.createIRI(countryUri)
                );
            });

            final var model = modelBuilder.build();
            connection.add(model);
            connection.commit();
        }
    }

    private void importInflationData(@NonNull final List<InflationDataJson> inflationData) {
        try(final var connection = repository.getConnection()) {
            connection.begin();

            final var modelBuilder = new ModelBuilder();
            final var valueFactory = connection.getValueFactory();

            inflationData.forEach(entry -> {
                final var countryUri = toCountryUri(entry.getCountryCode());
                final var yearUri = toYearUri(entry.getYear().toString());
                final var inflationUri = toInflationUri(UUID.randomUUID().toString());

                addBaseModel(
                        modelBuilder,
                        valueFactory,
                        countryUri,
                        entry.getCountry(),
                        yearUri,
                        entry.getYear()
                );

                modelBuilder.add(
                        valueFactory.createIRI(inflationUri),
                        valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                        valueFactory.createIRI(toUri(Ontology.Classes.INFLATION))
                );
                modelBuilder.add(
                        valueFactory.createIRI(inflationUri),
                        valueFactory.createIRI(toUri(Ontology.Properties.INFLATION_VALUE)),
                        entry.getInflation()
                );
                modelBuilder.add(
                        valueFactory.createIRI(inflationUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_IN_YEAR)),
                        valueFactory.createIRI(yearUri)
                );
                modelBuilder.add(
                        valueFactory.createIRI(inflationUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_FOR_COUNTRY)),
                        valueFactory.createIRI(countryUri)
                );
            });

            final var model = modelBuilder.build();
            connection.add(model);
            connection.commit();
        }
    }

    private void importPopulationData(@NonNull final List<PopulationDataJson> populationData) {
        try(final var connection = repository.getConnection()) {
            connection.begin();

            final var modelBuilder = new ModelBuilder();
            final var valueFactory = connection.getValueFactory();

            populationData.forEach(entry -> {
                final var countryUri = toCountryUri(entry.getCountryCode());
                final var yearUri = toYearUri(entry.getYear().toString());
                final var populationUri = toPopulationUri(UUID.randomUUID().toString());

                addBaseModel(
                        modelBuilder,
                        valueFactory,
                        countryUri,
                        entry.getCountry(),
                        yearUri,
                        entry.getYear()
                );

                modelBuilder.add(
                        valueFactory.createIRI(populationUri),
                        valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                        valueFactory.createIRI(toUri(Ontology.Classes.POPULATION))
                );
                modelBuilder.add(
                        valueFactory.createIRI(populationUri),
                        valueFactory.createIRI(toUri(Ontology.Properties.POPULATION_VALUE)),
                        entry.getPopulation()
                );
                modelBuilder.add(
                        valueFactory.createIRI(populationUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_IN_YEAR)),
                        valueFactory.createIRI(yearUri)
                );
                modelBuilder.add(
                        valueFactory.createIRI(populationUri),
                        valueFactory.createIRI(toUri(Ontology.Relations.DETERMINED_FOR_COUNTRY)),
                        valueFactory.createIRI(countryUri)
                );
            });

            final var model = modelBuilder.build();
            connection.add(model);
            connection.commit();
        }
    }

    private void addBaseModel(@NonNull final ModelBuilder modelBuilder,
                              @NonNull final ValueFactory valueFactory,
                              @NonNull final String countryUri,
                              @NonNull final String countryName,
                              @NonNull final String yearUri,
                              @NonNull final Integer yearValue) {
        modelBuilder.add(
                valueFactory.createIRI(countryUri),
                valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                valueFactory.createIRI(toUri(Ontology.Classes.COUNTRY))
        );
        modelBuilder.add(
                valueFactory.createIRI(countryUri),
                valueFactory.createIRI(toUri(Ontology.Properties.COUNTRY_NAME)),
                countryName
        );
        modelBuilder.add(
                valueFactory.createIRI(yearUri),
                valueFactory.createIRI(Ontology.RdfSyntax.RDF_TYPE),
                valueFactory.createIRI(toUri(Ontology.Classes.YEAR))
        );
        modelBuilder.add(
                valueFactory.createIRI(yearUri),
                valueFactory.createIRI(toUri(Ontology.Properties.YEAR_VALUE)),
                yearValue
        );
    }

    private String toAreaUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.AREA, id);
    }

    private String toCountryUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.COUNTRY, id);
    }

    private String toGdpUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.GDP, id);
    }

    private String toInflationUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.INFLATION, id);
    }

    private String toPopulationUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.POPULATION, id);
    }

    private String toYearUri(@NonNull final String id) {
        return String.format(SPECIFIC_RESOURCE_TEMPLATE, Ontology.Classes.YEAR, id);
    }

    private String toUri(@NonNull final String string) {
        return BASE_URI + string;
    }

}
