package de.fabianmittmann.semantic.web.project.datareader;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;

/**
 * @author Fabian Mittmann
 */
@Data
@Setter(AccessLevel.NONE)
public class PopulationDataJson {

    @JsonProperty("Country Code")
    private String countryCode;

    @JsonProperty("Country Name")
    private String country;

    @JsonProperty("Year")
    private Integer year;

    @JsonProperty("Value")
    private Long population;

}
